const BASE_URL = "https://635f4b0b3e8f65f283b01012.mockapi.io"
var editID = null;

// lấy danh sách từ axios
function getTodo (){

    // bat loading
    onLoading();
    // goi axios
    axios({
        url: `${BASE_URL}/todo`,
        method: "GET"
    })
    .then(function(res){
        console.log('res: ', res);
        // goi thanh cong tat loading
        offLoading();
        // render danh sach ra giao dien
        renderTodo(res.data);

    })
    .catch(function(err){
        console.log('err: ', err);
        offLoading();
    })
}
// lay danh sach
getTodo();

// them todo
function addTodo() {
    // tao bien chua toan bo thong tin nguoi dung nhap
    var data = getInfo();
    // tao lop doi tuong
    var newTodo = {
        name: data.name,
        desc: data.desc,
    }
    // bat loading
    onLoading();
    // goi axios
    axios({
        url: `${BASE_URL}/todo`,
        method: "POST",
        data: newTodo,
        isComplete: true,
    })
    .then(function(res){
        console.log('res: ', res);
        // tat loading
        offLoading();
        // goi lai danh sach sau khi them ra giao dien
        getTodo();
        // reset o input
        resetInput();

    })
    .catch(function(err){
        console.log('err: ', err);
        offLoading();
    })
}

// xoa todo
function delTodo(id){
    // bat loading
    onLoading();
    // goi axios
    axios({
        url: `${BASE_URL}/todo/${id}`,
        method: "DELETE"
    })
    .then(function(res){
        console.log('res: ', res);
        // xoa thanh cong tat loading
        offLoading();
    // in danh sach sau khi xoa ra giao dien
        getTodo();

    })
    .catch(function(err){
        console.log('err: ', err);
        offLoading();

    })
}

// sua todo
function editTodo(id){
    // bat loading
    onLoading();
    // goi axios
    axios({
        url: `${BASE_URL}/todo/${id}`,
        method: "GET"
    })
    .then(function(res){
        console.log('res: ', res);
        // goi thanh cong tat loading
        offLoading();
        // gan user can sua
        var userEdit = res.data;
        console.log('userEdit: ', userEdit);
        // show user can sua 
        document.getElementById("name").value = userEdit.name;
        document.getElementById("desc").value = userEdit.desc;
        // id cua user can sua
        editID = userEdit.id;
        console.log('editID: ', editID);
        
    })
    .catch(function(err){
        console.log('err: ', err);
        offLoading();
        
    })

};

// cap nhat todo
function updateTodo(){
    // tao bien chua thong tin nguoi dung nhap
    var data = getInfo();
    // bat loading
    onLoading();
    // goi axios
    axios({
        url: `${BASE_URL}/todo/${editID}`,
        method: "PUT",
        data: data,
    })
    .then(function(res){
        console.log('res: ', res);
        // goi thanh cong tat loading
        offLoading();
        // in danh sach sau khi cap nhat ra giao dien
        getTodo();
        // reset o input
        resetInput();
        
    })
    .catch(function(err){
        console.log('err: ', err);
        offLoading();
    })
}

