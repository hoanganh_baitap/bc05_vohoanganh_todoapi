// lay thong tin 
function getInfo(){
    var name = document.getElementById("name").value;
    var desc = document.getElementById("desc").value;

   return {
       name: name,
       desc: desc,
   }
}  

// render todo
function renderTodo(todo){
    var contentHTML = "";
    // duyet mang
    todo.forEach(item => {
        var contentTr = `<tr>
        <td>${item.id}</td>
        <td>${item.name}</td>
        <td>${item.desc}</td>
        <td>${item.isComplete}</td>
        <td>
        <button class="btn btn-warning mb-3" onclick="editTodo('${item.id}')">Edit</button>
        <button class="btn btn-danger" onclick="delTodo('${item.id}')">Delete</button>

        </td>

        <tr>`
        contentHTML += contentTr;
    });
    // show len giao dien
    document.getElementById("tbody").innerHTML = contentHTML;
}

// reset input
function resetInput(){
document.getElementById("name").value = "";
document.getElementById("desc").value = "";
}

// bat loading
function onLoading(){
    document.getElementById("loading").style.display = "flex";

}

// tat loading
function offLoading(){
    document.getElementById("loading").style.display = "none";

}